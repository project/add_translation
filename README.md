# Add translations of nodes in another language.

The add translation module creates translated nodes in bulk for a language,
instead of manually translating each individual node.

## Installation Steps

- Download `add_translation` module and place it in modules/custom directory.
- Enable the module `add_translation` from Extends.
- It will automatically enable the dependent modules: Language
  Locale, and Content Translation.
- Go to the language page and add the required `languages`
  (/admin/config/regional/language)
- Go to the `Content language and translation page`
  (/admin/config/regional/content-language) and
  select the content types that you want to enable translation for.
- Once you have enabled translation for the content types and added the
  translation languages, you can start configure this module and
  click the `Add Translation` button to add the translation.

## How to confgure the Module
- Go to the configuration -> `Configure Bulk Translation`
  (/admin/config/add-translation/settings)
- Choose the `language`, `content type` and `status`
  from the dropdown and save the configuration.

![Screenshot](screenshot.png)

- Finally click the button `Add Translation` from Bulk Translation Tab.
  It will generate the translated nodes in bulk for the selected language
  and content type.

![Screenshot](screenshot-add.png)

- It runs in batches to translate the nodes.
